#include "BestFit.h"

BestFit::BestFit()
{
    unsigned int num_pages = TOTAL_PAGE_NUM;
    unsigned int blockSize = 1;
    unsigned int startPPN = 0;

    _head = _tail = nullptr;

    while (num_pages > 0)
    {
        struct BestFit_Block* new_block = new struct BestFit_Block;

        new_block->startPPN = startPPN;
        new_block->startVPN = 0;
        new_block->allocated = false;
        new_block->num_pages_allocated = 0;
        new_block->_PageTable = nullptr;
        new_block->next = nullptr;

        if (blockSize <= num_pages)
        {
            new_block->blockSize = blockSize;
            startPPN += blockSize;
            num_pages -= blockSize;
        }
        else
        {
            new_block->blockSize = num_pages;
            startPPN += num_pages;
            num_pages = 0;
        }

        blockSize *= 2;

        if (_head == nullptr)
            _head = new_block;
        if (_tail == nullptr)
            _tail = new_block;
            else
            {
                _tail->next = new_block;
                _tail = new_block;
            }
            
    }

}

BestFit::~BestFit()
{
    while (_head != nullptr)
    {
        struct BestFit_Block* temp = _head;
        _head = _head->next;
        delete temp;
    }
}

bool BestFit::Do_Allocate(unsigned int START_VPN, unsigned int NUM_REQ_PAGES)
{
    struct BestFit_Block * target = nullptr;
    for (struct BestFit_Block *i = _head; i != nullptr; i = i->next)
    {
        if ((i->blockSize >= NUM_REQ_PAGES) && (!i->allocated))
        {
            if ((target == nullptr) || (i->blockSize < target->blockSize))
                target = i;
        }
    }

    if (target != nullptr)
    {
        target->allocated = true;
        target->startVPN = START_VPN;
        target->num_pages_allocated = NUM_REQ_PAGES;

        _PageTable = new SPTE *[NUM_REQ_PAGES];
        for (unsigned int i = 0; i < NUM_REQ_PAGES; i++)
        {
            _PageTable[i] = new SPTE;
            _PageTable[i]->nPPN = target->startPPN + i;
            _PageTable[i]->nBlockSize = NUM_REQ_PAGES - i;
        };

        target->_PageTable = _PageTable;

        return true;
    }
    else
        return false;
}

SPPTE* BestFit::Get_PageTable()
{
    return _PageTable;
}

int BestFit::Get_Fragmentation(unsigned int START_VPN)
{
    for (struct BestFit_Block *i = _head; i != nullptr; i = i->next)
    {
        if ((i->allocated) && (i->startVPN == START_VPN))
            return i->blockSize - i->num_pages_allocated;
    }
    return -1;
}

bool BestFit::Do_Deallocate(unsigned int START_VPN, unsigned int NUM_REQ_PAGES)
{
    bool de_allocated = false;
    for (struct BestFit_Block *i = _head; i != nullptr; i = i->next)
    {
        if ((i->allocated) && (i->startVPN == START_VPN))
        {
            i->allocated = false;
            i->startVPN = 0;
            for (unsigned int j = 0; j < i->num_pages_allocated; j++)
                delete i->_PageTable[j];

            delete[] i->_PageTable;

            de_allocated = true;
        }
    }

    return de_allocated;
}

void BestFit::dump()
{
    std::cout << "Dump all block of BestFit -------------------------------------->" << std::endl;
    std::cout.width(12); 
    std::cout << "startPPN";
    std::cout.width(12); 
    std::cout << "blockSize";
    std::cout.width(12); 
    std::cout << "allocated_size";
    std::cout.width(12); 
    std::cout << "allocated?";
    std::cout.width(12); 
    std::cout << "startVPN";
    std::cout << std::endl;

    for (struct BestFit_Block *i = _head; i != nullptr; i = i->next)
    {
        std::cout.width(12); 
        std::cout << i->startPPN;
        std::cout.width(12); 
        std::cout << i->blockSize;
        std::cout.width(12);
        std::cout << i->num_pages_allocated;
        std::cout.width(12); 
        std::cout << i->allocated;
        std::cout.width(12); 
        std::cout << i->startVPN;
        std::cout << std::endl;
    }

    std::cout << std::endl << std::endl;

}