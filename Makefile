.PHONY: run

all:
	g++ -g -Wall -std=c++11 -D"CHANGE_NUM_PAGES" -D"NUM_PAGES = 1000" -o  BestFitTest BestFit.cpp main.cpp

run: 
	./BestFitTest > output.txt

clean:
	rm -f output.txt output BestFitTest
