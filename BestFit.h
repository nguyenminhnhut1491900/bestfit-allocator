#ifndef BEST_FIT_H
#define BEST_FIT_H

#include <iostream>
#include <iomanip>
#include "Top.h"

#define TOTAL_PAGE_NUM 65536 //256
// #define PAGE_SIZE 4 * (2 << 10)

struct BestFit_Block
{
    unsigned int startPPN;
    unsigned int blockSize;
    unsigned int startVPN;

    bool allocated;

    SPPTE* _PageTable;
    unsigned int num_pages_allocated;

    BestFit_Block *next;
};

class BestFit
{
    private:   
        struct BestFit_Block * _head, *_tail;
        SPPTE* _PageTable;

    public:
        BestFit();
        ~BestFit();

        bool Do_Allocate(unsigned int START_VPN, unsigned int NUM_REQ_PAES);
        bool Do_Deallocate(unsigned int START_VPN, unsigned int NUM_REQ_PAGES);

        SPPTE* Get_PageTable();
        int Get_Fragmentation(unsigned int START_VPN);


        void dump();

};


#endif