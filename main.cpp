#include "BestFit.h"
#include <chrono>
#include <fstream>

int main(){
    #ifdef CHANGE_NUM_PAGES
        int num_pages=NUM_PAGES;
    #endif
    #ifndef CHANGE_NUM_PAGES
        int num_pages=100; 
    #endif
    BestFit * cpBestFit = new BestFit;
    std::ofstream filecsv;
    filecsv.open("bestfit_timerun.csv",std::ios_base::app);
    #ifdef SCENARIO_1
        string scenario = "SCENARIO_1";
        int NUM_ALLOCATE= 2;
    #endif
    #ifdef SCENARIO_2
        string scenario = "SCENARIO_2";
        int NUM_ALLOCATE= 2;
    #endif
    #ifdef SCENARIO_5
        string scenario = "SCENARIO_5";
        int NUM_ALLOCATE= 4;
    #endif
    #ifdef SCENARIO_6
        string scenario = "SCENARIO_6";
        int NUM_ALLOCATE= 3;
    #endif 
    #ifdef SCENARIO_7
        string scenario = "SCENARIO_7";
        int NUM_ALLOCATE= 1;
    #endif
    cpBestFit->dump();
    auto start = std::chrono::high_resolution_clock::now();
    auto end = std::chrono::high_resolution_clock::now();
    auto time_run = std::chrono::duration_cast<std::chrono::microseconds>(end-start).count();
    std::cout << "(*) Allocation: --------------------------------" << std::endl;
    for (int i = 0; i < NUM_ALLOCATE; i++)
    {
    
    start = std::chrono::high_resolution_clock::now();
    cpBestFit->Do_Allocate(0x30+i*num_pages+1, num_pages);
    //cpBestFit->dump();
    
    // cpBestFit->dump();
    filecsv<<scenario<<",";
    filecsv<<(48+i*num_pages+1)<<",";
    filecsv<<num_pages<<",";

    end = std::chrono::high_resolution_clock::now();
    time_run = std::chrono::duration_cast<std::chrono::microseconds>(end-start).count();

    if (time_run < 1){
        time_run = std::chrono::duration_cast<std::chrono::nanoseconds> (end-start).count();
         filecsv<<time_run<<",ns,";

    }
    else
         filecsv<<time_run<<",ms," ;
    
    filecsv<<cpBestFit->Get_Fragmentation(0x30+i*num_pages+1)<<"\n";
    cout<<cpBestFit->Get_Fragmentation(0x30+i*num_pages+1)<<"\n";
    }
    filecsv.close();
    std::cout << "(*) De-allocation: --------------------------------" << std::endl;
    auto start1 = std::chrono::high_resolution_clock::now();

    cpBestFit->Do_Deallocate(0x30, 100);
    // cpBestFit->dump();

    cpBestFit->Do_Deallocate(0x50, 100);
    // cpBestFit->dump();   

    cpBestFit->Do_Deallocate(0x70, 100);
    // cpBestFit->dump();

    auto end1 = std::chrono::high_resolution_clock::now();
    auto time_run1 = std::chrono::duration_cast<std::chrono::microseconds>(end1-start1).count();

    if (time_run1 < 1){
        time_run1 = std::chrono::duration_cast<std::chrono::nanoseconds> (end1-start1).count();
        std::cout << "   (-) Time execute: "<< std::fixed << std::setprecision(2) << time_run1  << " nanoSec."<< std::endl;

    }
    else
        std::cout << "   (-) Time execute: "<< std::fixed << std::setprecision(2) << time_run1  << " microSec."<< std::endl;


    delete(cpBestFit);

    return 0;
}